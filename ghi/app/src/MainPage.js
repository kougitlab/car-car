import React, {useState, useEffect} from 'react'
import "./css/styles.css"

function ReviewColumn(props){
  return (
    <div className="col">
      {props.list.map(data => {
        return (
          <div key={data.id} className="card mb-3 shadow">
            <h5 className="card-img-top" />
              {data.description}
            <div className="card-body">
              <h6 className="cart-subtitle mb-2 text-muted"/>
                {data.stars}
              <h6 className="cart-subtitle mb-2 text-muted"/>
              {data.date}
              <h6 className="cart-subtitle mb-2 text-muted"/>
              {data.contact_info}
            </div>
            </div>
            );
          })}
    </div>
  )
}

function MainPage() {
  const [reviewColumns, setReviewColumns]= useState([[],[],[]])

  const fetchData = async()=>{
    const url= 'http://localhost:8100/api/reviews'
    try{
      const columns=[[],[],[]];
      let i=0
      const response=await fetch(url)
      if (response.ok){
        const data=await response.json();
        for (let review of data.reviews){
          columns[i].push(review);
          i=i+1;
          if (i>2){
            i=0
          }
        }
      }

        setReviewColumns(columns)
    } catch (e) {
      console.error(e);
    }
  }
  useEffect(()=>{
    fetchData();
  }, []);



  return (
    <div className="main-container">
      <p id="car-p">CarCar</p>
      <p className="car-main-description"></p>
          <div className="award-gallery-container">
            <img className="img-award-main" src="https://www.bestdealershipawards.com/images/BestDealershipAwardsLogo.png"></img>
            <img className="img-award-4-main" src="https://upload.wikimedia.org/wikipedia/en/e/ed/Nobel_Prize.png"></img>
            <img className="img-award-2-main" src="https://di-uploads-pod31.dealerinspire.com/vanhornautomotivegroupinc/uploads/2022/03/2022-DealerRater-Award-1024x1024.jpg"></img>
            <img className="img-award-3-main" src="https://pictures.dealer.com/t/toyotaofwatertown/1078/1606ed777ce0b7c327f38ab75689459ex.jpg?impolicy=downsize&w=568"></img>
            <img className="img-award-5-main" src="http://www.cnanational.com/sites/default/files/2022-07/Dealers%20Choice%20Award%202022_award.png"></img>
          </div>
          <p className="car-main-description">
            Award winning dealership.
            You drive it, we win it.
          </p>
        <div>
          
          <p className="review-main-description"></p>
            {reviewColumns.map((reviewList, index)=>{
              return(
                <ReviewColumn key={index} list={reviewList}/>
              )
            })}
        </div>

    </div>
  );
}

export default MainPage;
