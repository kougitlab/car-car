import { useEffect, useState } from "react";
import { Link } from 'react-router-dom';

function ModelList() {
    const [models, setModels] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect(() => {
    fetchData();
    }, []);

    return (
    <div className="container">
        <h1>List of Vehicle Models</h1>
        <div className="row">
            <div className="col-sm">
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
                </thead>
                <tbody>
                {models.map(model => {
                    return (
                    <tr key={model.id}>
                        <td>{ model.name }</td>
                        <td>{model.manufacturer.name}</td>
                        <td>
                            <img
                            src={ model.picture_url }
                            className="img-thumbnail"
                            alt="new"
                            />
                        </td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
            </div>
        </div>
        <Link to="/models/new" className="mt-3 btn btn-primary mb-3">Add a new model</Link>
    </div>
    );
    }

export default ModelList;
