# CarCar

Team:

* Derek 
* Edward 
* Kevin 

## **Design**
![CarCar design diagram](http://gyazo.com/c7f47ca89a22b860c6e07d43c0db7fe7)

<p> CarCar is a three part application with inventory, sales, and service microservices. The inventory microservice tracks the dealership and the various manufacturers, vehicle models, and automobiles they have in inventory. The elements within the inventory bounded context rely on each other to create the main model of the automobile. An automobile in inventory must be tied to a vehicle model in the database, and the vehicle model must be tied to a manufacturer. The service microservice deals with cars coming into the dealership for service appointments. Though the cars coming into the dealership for service do not have to have been purchased from the dealer, if a car was at any time in inventory and purchased from the dealership, it will receive VIP treatment for its service appointment. Within the service bounded context, there exist three models tied together: the technician, automobile value object, and the service appointment. There must be a technician assigned to each service appointment, and each service appointment checks its VIN against the VINs of those in the automobile value objects to determine if that car will receive a VIP treatment. The sales microservice handles the data from the customer, sales person, price, and the record of the sale. The sales is its own bounded context but depends on the inventory for the information about the automobiles. The customer will be able to select an unsold automobile from the inventory, and with a sales person, they will be able to create a sale/sale record. The sales history relies on the sale records to show all the sales from a select sales person.</p>

## **Start-up Instructions**

1. Fork and clone the repository at <a>https://gitlab.com/derekwangg/project-beta</a>
2. Ensure docker desktop is running.
3. Create a volume named beta-data to store local data using the command `docker volume create beta-data` in your terminal.
4. In your terminal, run `docker-compose build` and `docker-compose up` to build and run your containers and images for the application.
5. Refer to the sections below for detailed information and instructions for the CRUD routing for testing the back end using Insomnia for each microservice.
6. Navigate to <a> localhost:3000 </a> to utilize the REACT front end with the navigation bar.
-----------------------------
## Inventory microservice

Port http://localhost:8100

For the inventory microservice in the project, the main purpose was to be able to create three resources: manufacturer, vehicle model, and the automobile.

There were three models, the manufacturer, vehicle model, and automobile. The manufacturer only required a name attribute, while the vehicle model requried a name and picture, while having the manufacturer as the foreign key relationship. The Automobilemodel had the color, year, vin, and model attribute. This automobile was what both microservices Sales and Services were interested in, which was the reason the automobile value object was created in the other microservices.

The inventory had several single object api's, as well as collection api's, meaning the resources were able to GET PUT POST DELETE. Unique URLs were created for the different methods.

The React frontend component integrates with the Django RESTful api backend portion. The create SPA frontend uses the data from the RESTful api's to be able to make forms that create automobiles, vehicles, and manufacturers. Additionally, list SPA fronted were made, that also relied on the RESTful URLs, specifcally the collection api's so that data can be GET and POST. Hooks were used, specifically useState and useEffect to allow for dynamic updating the data when needed, as well as fetching data and updating the dom, to further increase the dynamic aspect of the React frontend.

# **URLS and Ports for Inventory microservice**

## **Manufacturers**
| Action              | Method | URL                                    |
| ------------------- | ------ | -------------------------------------- |
| List Manufacturers   | GET    | http://localhost:8100/api/manufacturers/ |
| Create a manufacturer | POST   | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET   | http://localhost:8100/api/manufacturers/:id/ |
| Update a specific manufacturer | PUT   | http://localhost:8100/api/manufacturers/:id/ |
| Delete a specific manufacturer | DELETE   | http://localhost:8100/api/manufacturers/:id/ |


Creating and updating a manufacturer requires only the manufacturer's name.

```
{
  "name": "Chrysler"
}
```

The return value of creating, getting, and updating a single manufacturer is its name, href, and id.

```
{
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Chrysler"
}
```

The list of manufacturers is a dictionary with the key "manufacturers" set to a list of manufacturers.

```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}
```

## **Vehicle models**

| Action              | Method | URL                                    |
| ------------------- | ------ | -------------------------------------- |
| List vehicle models   | GET    | http://localhost:8100/api/manufacturers/ |
| Create a manufacturer | POST   | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET   | http://localhost:8100/api/manufacturers/:id/ |
| Update a specific manufacturer | PUT   | http://localhost:8100/api/manufacturers/:id/ |
| Delete a specific manufacturer | DELETE   | http://localhost:8100/api/manufacturers/:id/ |

Creating and updating a vehicle model requires the model name, a URL of an image, and the id of the manufacturer.

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```

Updating a vehicle model can take the name and/or the picture URL.
```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}
```

Getting the detail of a vehicle model, or the return value from creating or updating a vehicle model, returns the model's information and the manufacturer's information.

```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```


Getting a list of vehicle models returns a list of the detail information with the key "models".
```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```

## **Automobile information**

***Note: The identifiers for automobiles in this API are not integer ids. They are the Vehicle Identification Number (VIN) for the specific automobile.***

| Action              | Method | URL                                    |
| ------------------- | ------ | -------------------------------------- |
| List automobiles   | GET    | http://localhost:8100/api/automobiles/ |
| Create an automobile | POST   | http://localhost:8100/api/automobiles/ |
| Get a specific automobile | GET   | http://localhost:8100/api/automobiles/:vin/ |
| Update a specific automobile | PUT   | http://localhost:8100/api/automobiles/:vin/ |
| Delete a specific automobile | DELETE   | http://localhost:8100/api/automobiles/:vin/ |

You can create an automobile with its color, year, VIN, and the id of the vehicle model.

```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```

As noted, you query an automobile by its VIN. For example, you would use the URL

http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/

to get the details for the car with the VIN "1C3CC5FB2AN120174". The details for an automobile include its model and manufacturer.

```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "yellow",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  }
}
```

You can update the color and/or year of an automobile.

```
{
  "color": "red",
  "year": 2012
}
```

Getting a list of automobiles returns a dictionary with the key "autos" set to a list of automobile information.
```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      }
    }
  ]
}
```
------------------------------------------------------
# **Service microservice**

## **Utilizing the back end with Insomnia**
-------------------------------------
The Services microservice is bound to `localhost:8080`. It contains three models. Users will be able to use insomnia to plug in the URLs along with the different types of requests and requests bodies to test functionality and create data.

----------------------------------------
### **The Technician Model**

<p>The Technician model is used for GET and POST requests and contains information regarding the technician name, employee number, and id. The technician model serves as a foreign key for the Appointments model, as there must be a valid technician within the database to perform the service. Both the GET and POST requests can be accessed in insomnia at the <a>http://localhost:8080/api/technicians/ </a>. A GET request will return an array of the list of technicians displaying each technician's name, employee number, and id. A POST request must contain the technician name and employee number. Upon a successful POST request, the return response will include the technician name, employee number, and id. Please see below for sample requests and responses.</p>

Technicians
| Action              | Method | URL                                    |
| ------------------- | ------ | -------------------------------------- |
| List Technician  | GET    | http://localhost:8080/api/technicians/ |
| Create Technician | POST   | http://localhost:8080/api/technicians/ |
-------------------------------------------------------------
Sample return response for GET request:
```
{
	"technicians": [
		{
			"name": "Bob",
			"employee_number": "5",
			"id": 1
		},
		{
			"name": "test",
			"employee_number": "6",
			"id": 2
		},
	]
}
```

Sample JSON body for POST request:
```
{
	"name":"derek",
	"employee_number": "45667"
}
```
Sample return response to successful POST request:
```
{
	"name": "derek",
	"employee_number": "45667",
	"id": 5
}
```
---------------------------------------
### **The Automobile Value Object Model**

<p>The Automobile Value Object model polls the inventory service at <a> http://inventory-api:8000/api/automobiles/</a> to recieve a list of automobiles in the database to create and store entries for later use within the Services microservice to determine the vip status of service appointments(more detail in description of the appointment model). The only information needed in this current build is the automobile vin, but the poller will store other data regarding the color, year, and detail href for posterity or future builds and features of the application.</p>

------------------------------
### **The Appointment Model**

<p>The Appointment model is used for GET, POST, PUT, and DELETE requests and contains information regarding the automobile vin, customer name, date of the service appointment, time of the service appointment, reason for the service appointment, completed status of the service appointment(true or false), a foreign key with the name of the technician to whom the appointment is assigned, id of the appointment, and vip status(whether or not the automobile was purchased from the dealership)(True or false). This is the main model which houses the primary utility and functionality of the Service microservice. Upon creation of an appointment, the view function will first check if the VIN for the automobile in the appointment matches a VIN in the Automobile Value Object database within the Service microservice, which polls from inventory(see Automobile Value Object Model section). If there is a match, then the VIP field will be updated to True, as this indicates that the vehicle was at one time in inventory and was purchased from the dealership. If there is no match, then the VIP field will default to False. The "completed" field will also default to false upon creation and will need to be updated or deleted at a later time upon cancellation or completion. The GET and POST requests can be accessed at <a> http://localhost:8080/api/appointments/ </a>. A GET request will return an array of appointments with the vip, vin, customer name, date, time, reason, completed, technician, and id. A POST request will require a vin, customer name, date, time, reason, and a valid technician. Upon a successfull post request, the return response will include vip, vin, customer name, date, time, reason, completed, id, and technician. Please see below for sample requests and responses.</p>

Appointments
| Action              | Method | URL                                    |
| ------------------- | ------ | -------------------------------------- |
| List Appointments  | GET    | http://localhost:8080/api/appointments/ |
| Create Appointment | POST   | http://localhost:8080/api/appointments/ |

Sample return response for a GET request:
```
{
	"appointments": [
		{
			"vip": true,
			"vin": "123456789",
			"customer_name": "Adam",
			"date": "2023-03-15",
			"time": "12:00:00",
			"reason": "oil change",
			"completed": true,
			"id": 15,
			"technician": "Bob"
		},
		{
			"vip": false,
			"vin": "1234567890",
			"customer_name": "eddy",
			"date": "2023-03-15",
			"time": "12:00:00",
			"reason": "oil change",
			"completed": true,
			"id": 1,
			"technician": "Bob"
		},
	]
}
```
Sample JSON body for POST request:
```
{
	"vin": "1234567890",
	"customer_name":"Adam",
	"date": "2023-03-15",
	"time": "12:00:00",
	"reason": "tuneup",
	"technician": "derek"
}
```
Sample return response for POST request:
```
{
	"vip": false,
	"vin": "1234567890",
	"customer_name": "Adam",
	"date": "2023-03-15",
	"time": "12:00:00",
	"reason": "tuneup",
	"completed": false,
	"id": 23,
	"technician": "derek"
}
```
<p>DELETE and PUT requests will need to be made at the url <a> http://localhost:8080/api/appointments/< int:id >/
</a>with the int id being the assigned id of a specific service appointment. A successful DELETE request will return a response with "deleted":true. A PUT request will require similar structure to the POST request, which will allow any of the fields with the exception of id to be updated. Users will only have to include the fields that they wish to edit within the request and may exclude anything not being updated. This request will be mainly used to update the completed status of an appointment. Please see below for example requests and outputs.</p>
Sample return response for DELETE request:

Appointments
| Action              | Method | URL                                    |
| ------------------- | ------ | -------------------------------------- |
| Delete an Appointment  | DELETE    |  http://localhost:8080/api/appointments/< int:id >/ |
| Update an Appointment | PUT   |  http://localhost:8080/api/appointments/< int:id >/ |
```
{
	"deleted": true
}
```
Sample JSON body for PUT request:
```
{
	"completed": true
}
```
Sample return response for PUT request:
```
{
	"vin": "123456789",
	"customer_name": "Adam",
	"date": "2023-03-15",
	"time": "12:00:00",
	"reason": "oil change",
	"vip": true,
	"completed": true,
	"id": 14,
	"technician": "Bob"
}
```
<p>There is also an additional GET request in place to display the service history for a specific vehicle VIN. This can be accessed at <a>http://localhost:8080/api/appointments/history/< str:automobile_vo_vin >/</a>. The string automobile_vo_vin will be the specific VIN for the search request. The history element of the url is there in place so as to differentiate between this url path for service history and the url path for the DELETE and PUT requests. Please see below for sample requests and responses.</p>


Appointments
| Action              | Method | URL                                    |
| ------------------- | ------ | -------------------------------------- |
| List Appointment History | GET    |  http://localhost:8080/api/appointments/history/< str:automobile_vo_vin >/ |

Sample return response for GET request:
```
{
	"appointments": [
		{
			"vin": "123456789",
			"customer_name": "Adam",
			"date": "2023-03-15",
			"time": "12:00:00",
			"reason": "oil change",
			"vip": true,
			"completed": true,
			"id": 14,
			"technician": "Bob"
		},
		{
			"vin": "123456789",
			"customer_name": "Adam",
			"date": "2023-03-15",
			"time": "12:00:00",
			"reason": "oil change",
			"vip": true,
			"completed": false,
			"id": 15,
			"technician": "Bob"
		}
	]
}
```

----------------------------------------
## **Utilizing the front end Services with React**

<p>The React portion of the application is hosted on "localhost:3000". The React front end of the Services microservice contains three pages: The "Create a Technician" page at <a>http://localhost:3000/technicians/new</a>, the "Create a service appointment" page at <a>http://localhost:3000/appointments/new</a>, and the "List appointments" page at <a>http://localhost:3000/appointments/</a>. Navigation links to each of these pages are found under the "Services" dropdown section of the Navigation bar.</p>

--------------------------------------------
### **The Create a Technician Page**

<p>The "Create a Technician" page at <a>http://localhost:3000/technicians/new</a> allows users to create a new auto technician for service appointments by hooking it up to a POST request to <a>http://localhost:8080/api/technicians/</a>. The required fields include name and employee number fields. Upon a successful submission, the form will disappear and a success alert will be displayed. </p>

---------------------------------------------
### **The Create a service appointment page**
<p> The "Create a service appointment" page at <a>http://localhost:3000/appointments/new</a> will allow users to create a new service appointment. The required fields include a VIN, Customer Name, Date of the appointment, Time of the appointment, Reason for appointment, and the Technician whom the appointment is assigned to. In order to populate the technician dropdown, a fetch function is created to fetch all the technicians from <a>http://localhost:8080/api/technicians/</a> to populate the dropdown. A submission will send a POST request to <a> http://localhost:8080/api/appointments/ </a>. Upon a successful submission, the form will disappear and a success alert will be displayed. The appointment will now be viewable in the list appointments page.</p>

------------------------------------------
### **The List service appointments page**
<p> The "List appointments" page at <a>http://localhost:3000/appointments/</a> will allow users to view both all pending service appointments as well as filter by VIN to view the full service history of completed and pending service appointments. Upon the first loadup of the page, it will send a fetch request to <a> http://localhost:8080/api/appointments/ </a> and then populate a list of all pending service appointments for all VINs. The information displayed with be the VIP Status(whether the vehicle was purchased from the dealership and at any time in inventory), the VIN, Customer Name, Date, Time, Technician, Reason, and Status. There are two buttons under the Update Status table header which will allow a user to either cancel an appointment to remove it from the database or update its status to completed. Each of these buttons will then send the corresponding DELETE or PUT request to the unique appointment url. Both of these buttons will then remove the appointment from the list view. At the top of the page, there is a search bar where users will be able to search by VIN to display a vehicles full service appointment history. This will create a fetch request to the unique history url for each VIN and display both completed and pending Service appointments. Beneath the VIN search bar there is also an "All Appointments" button that, upon clicking, will return to the original full view of all pending appointments for all VINs</p>

------------------------------------
## Sales microservice

Port http://localhost:8090

Explain your models and integration with the inventory
microservice, here.

For the sales portion of the project, the backend portions were hooked up properly in the sales_project settings.py and urls.py. Models were created for customer, sales person, sale, and the value object automobileVO. The sale had to have multiple foreign keys since it relied on the information from sales person, a customer, and automobile to create an actual sale.

For the inventory back end integration to the sales microservice, the poller.py file was done so that the data from the automobile inventory was properly polling to the sale microservice, and the information was used in the automobileVO model we created. The inventory microservice itself had many RESTful single objects, where they were able to GET DELETE PUT POST, and that helped create the manufacturer, vehicle mode, which combined to create the automobile resource in the inventory. Which we need data from.

We created several collection api views functions, in addition to their encoders. We made it so that the list sale, list customer, list sales person api's were able to GET and POST. The sale collection api was the most difficult as it required other relationships for data, so it was more complex. Then it was hooked up to the proper Urls and working in insomnia REST client.

For the front end portion, the urls from the backend api were used to retrieve the data that were then used to create the forms and list for the react front end portion. The collection RESTful apis created in the backend for the lists for customer, sales person, sale, specifically were used since they were able to GET and POST.
The useState and useEffect hooks were used to allow the frontend to be dynamic whenever users input changes, for the forms. The filter and map built in methods in jsx were used to properly get the list to show the desired data in several lists. Most of the forms had to populate the drop down select, in the sales record form specifically needed three drop downs, so that was more complex than the other forms. In addition, all the forms clear upon successful submission, with an alert that says form submitted.

## <u>The links for the forms and lists below:</u>
<div>History of sales for a sales person</div>
<div><a>http://localhost:3000/salesperson/saleshistory</a></div>
<div>Add a Sales Person</div>
<div><a>http://localhost:3000/salesperson/new</a><div>
<div>Add a potential customer</div>
<div><a>http://localhost:3000/customer/new</a></div>
<div>List Sales Record</div>
<div><a>http://localhost:3000/sales/</a><div>
<div>Add a new sale record</div>
<div><a>http://localhost:3000/sales/new</a></div>

-------------------------------------


# URLS and Ports for Sales microservice

## **Sales Person**
| Action              | Method | URL                                    |
| ------------------- | ------ | -------------------------------------- |
| List Sales Person   | GET    | http://localhost:8090/api/salesperson/ |
| Create Sales Person | POST   | http://localhost:8090/api/salesperson/ |

creating sales person requires only name and employee in the json body. it would look like this
```
{
    "name": "Kevin",
    "employee_number": 1
}
```
The return value of getting and creating is its name, employee_number, and id
```
{
  "name": "Kevin",
  "employee_number": 1,
  "id": 1
}
```
The list of sales persons is a dictionary with the key "salespersons" set to a list of sales persons.
```
{
  "salespersons": [
    {
        "name": "Kevin",
        "employee_number": "1",
        "id": 1
    }
]
}
```

-----------------------


## **Customer**
| Action              | Method | URL                                    |
| ------------------- | ------ | -------------------------------------- |
| List Customer   | GET    | http://localhost:8090/api/customer/ |
| Create Customer | POST   | http://localhost:8090/api/customer/ |

Creating a customer requires only name, address and phone number.
```
{
    "name": "Joe",
	"address": "123 real address",
	"phone_number": "123123123"
}
```
The return value of getting and creating is its name, address, phone number, and an id
```
{
	"name": "Joe",
	"address": "123 real address",
	"phone_number": "123123123",
	"id": 1
}
```
The list of customer is a dictionary with the key "customers" set to a list of customers.
```
{
	"customers": [
		{
			"name": "Customer 1",
			"address": "123 real address",
			"phone_number": "123123123",
			"id": 1
		},
		{
			"name": "Customer 2",
			"address": "123 realer address",
			"phone_number": "123123123",
			"id": 2
		},
		{
			"name": "Customer 3",
			"address": "123 I Live here Street",
			"phone_number": "1231231234",
			"id": 3
		}
	]
}
```

---------------------------

## **Sales Record**

| Action              | Method | URL                                    |
| ------------------- | ------ | -------------------------------------- |
| List Sale Record   | GET    | http://localhost:8090/api/sales/ |
| Create Sale Record | POST   | http://localhost:8090/api/sales/ |

Creating a sale record only requires auto, sales_person, customer, price.

```
{
  "auto": "1C3CC5FB2AN120174",
	"sales_person": "Kevin",
	"customer": "Joe",
	"price": "1234"
}
```

The return value for creating getting is its price, sales_person, customer, auto, id, and sales_person_employee_number.

```
{
	"sale": {
		"price": "1234",
		"sales_person": {
			"name": "Derek",
			"employee_number": "1",
			"id": 1
		},
    }
}
```

The list of sales is a dictionary with the key "sales" set to a list of sales.

```
{
	"sales": [
		{
			"price": "1234.00",
			"sales_person": "Kevin",
			"customer": "Joe",
			"auto": "1C3CC5FB2AN120174",
			"id": 1,
			"sales_person_employee_number": "1"
		},
    ]
}
```


