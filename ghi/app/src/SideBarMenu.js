import { NavLink } from 'react-router-dom';


function SideBarMenu() {
    return (
        <div className="container-fluid">
            <div className="row">
                <div id="sidebar" className="bg-dark col-auto min-vh-100 me-auto position-fixed">
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent1"></div>
                    <ul className="nav nav-pills flex-column navbar-nav me-auto">
                        <li className="nav-item text-white fs-4 my-1 ">
                            <a href="#" className="nav-link text-white fs-5" aria-current="page">
                                <NavLink className="nav-link text-white" aria-current="page" to="/">Home</NavLink>
                            </a>
                        </li>
                        <li className="nav-item text-white my-1">
                            {/* <a href="#" className="nav-link text-white fs-5" aria-current="page"> */}
                                <a className="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Inventory
                                </a>
                                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <NavLink className="dropdown-item" aria-current="page" to="/manufacturers">List Manufacturers</NavLink>
                                    <NavLink className="dropdown-item" aria-current="page" to="/manufacturers/new">Create a Manufacturer</NavLink>
                                    <div className="dropdown-divider"></div>
                                    <NavLink className="dropdown-item" to="/models/">List Vehicle Models</NavLink>
                                    <NavLink className="dropdown-item" aria-current="page" to="/models/new">Create a Vehicle Model</NavLink>
                                    <div className="dropdown-divider"></div>
                                    <NavLink className="dropdown-item" to="/automobiles/">List Automobile</NavLink>
                                    <NavLink className="dropdown-item" to="/automobiles/new">Create Automobile</NavLink>
                                </div>
                            {/* </a> */}
                        </li>
                        <li className="nav-item text-white fs-4 my-1">
                            {/* <a href="#" className="nav-link text-white fs-5" aria-current="page"> */}


                                <a className="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Services
                                </a>
                                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <NavLink className="dropdown-item" aria-current="page" to="/technicians/new">Create a technician</NavLink>
                                    <div className="dropdown-divider"></div>
                                    <NavLink className="dropdown-item" to="/appointments/">List Appointments</NavLink>
                                    <NavLink className="dropdown-item" aria-current="page" to="/appointments/new">Create a service appointment</NavLink>
                                </div>
                            {/* </a> */}
                        </li>
                        <li className="nav-item text-white fs-4 my-1">
                            {/* <a href="#" className="nav-link text-white fs-5" aria-current="page"> */}
                                <a className="nav-link dropdown-toggle text-white" href="#" id="SalesnavbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Sales
                                </a>
                                <div className="dropdown-menu" aria-labelledby="SalesnavbarDropdown">
                                    <NavLink className="dropdown-item" aria-current="page" to="/salesperson/saleshistory">Sale Person's Sale History</NavLink>
                                    <NavLink className="dropdown-item" aria-current="page" to="/salesperson/new">Add a Sales Person</NavLink>
                                    <div className="dropdown-divider"></div>
                                    <NavLink className="dropdown-item" aria-current="page" to="/customer/new">Add a Potential Customer</NavLink>
                                    <div className="dropdown-divider"></div>
                                    <NavLink className="dropdown-item" to="/sales/">List Sales Record</NavLink>
                                    <NavLink className="dropdown-item" to="/sales/new">Create Sale Record</NavLink>
                                </div>
                            {/* </a> */}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default SideBarMenu;

