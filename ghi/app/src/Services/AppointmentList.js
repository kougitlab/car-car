import { useEffect, useState } from "react";
import {Link} from 'react-router-dom';


function AppointmentList() {
    const[isSearching, setIsSearching]=useState("false")
    const[vin, setVIN]= useState("");
    const handleVinChange=(event)=>{
        const value=event.target.value;
        setVIN(value)
    }
    const [appointments, setAppointments] = useState([]);

    const fetchData = async () => {
        setIsSearching(false)
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    }

    useEffect(() => {
    fetchData();
    }, []);
    const handleSearch = async (event) => {
        event.preventDefault();
        setIsSearching(true)
        const historyUrl = `http://localhost:8080/api/appointments/history/${vin}/`;
        const vinResponse = await fetch(historyUrl);
        if (vinResponse.ok) {
            const data = await vinResponse.json();
            setAppointments(data.appointments);
            }

        }

    const handleDelete = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/`
        const response = await fetch(url, { method: "DELETE" });
        if (response.ok) {
            fetchData();
        }
    }
    const handleCompleted = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/`
        const fetchConfig = {
            method: "PUT",

            body: JSON.stringify({completed:"True"}),
            headers: {
                'Content-Type': 'application/json',
            },
        };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
        fetchData();
    }
}



    return (
    <div className="container">
        <form onSubmit={handleSearch} id="filter-by-VIN">
                <div className="form-floating mb-3">
                <input onChange={handleVinChange} value={vin}placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">VIN</label><button className="btn btn-outline-success">Search</button>
                </div>
        </form>
        <form onSubmit={fetchData} id="return-to-all">
                <div className="form-floating mb-3">
                    <button className="btn btn-outline-info">All Pending Appointments</button>
                </div>
        </form>
        <h1>Service appointments</h1>
        <div className="row">
            <div className="col-sm">
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>VIP Status</th>
                    <th>VIN</th>
                    <th>Customer Name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                    <th>Update Status</th>
                </tr>
                </thead>
                {!isSearching ?<tbody>
                {appointments.filter(appointment=>!appointment.completed).map(appointment => {
                    return (
                    <tr key={appointment.id}>
                        {appointment.vip ? <td>VIP</td>:<td>Peasant</td>}
                        <td>{ appointment.vin }</td>
                        <td>{ appointment.customer_name }</td>
                        <td>{ appointment.date }</td>
                        <td>{ appointment.time }</td>
                        <td>{ appointment.technician }</td>
                        <td>{ appointment.reason }</td>
                        {appointment.completed ? <td>Completed</td>:<td>Pending</td>}
                        {appointment.completed ? <td> </td>:<td>
                            <button
                                className="btn btn-danger"
                                onClick={() => handleDelete(appointment.id)}
                            >
                                Cancel
                            </button>
                            <button
                                className="btn btn-success"
                                onClick={() => handleCompleted(appointment.id)}
                            >
                                Completed
                            </button>
                        </td>}
                    </tr>
                    );
                })}
                </tbody>:
                <tbody>
                {appointments.map(appointment => {
                    return (
                    <tr key={appointment.id}>
                        {appointment.vip ? <td>VIP</td>:<td>Peasant</td>}
                        <td>{ appointment.vin }</td>
                        <td>{ appointment.customer_name }</td>
                        <td>{ appointment.date }</td>
                        <td>{ appointment.time }</td>
                        <td>{ appointment.technician }</td>
                        <td>{ appointment.reason }</td>
                        {appointment.completed ? <td>Completed</td>:<td>Pending</td>}
                        {appointment.completed ? <td> </td>:<td>
                            <button
                                className="btn btn-danger"
                                onClick={() => handleDelete(appointment.id)}
                            >
                                Cancel
                            </button>
                            <button
                                className="btn btn-success"
                                onClick={() => handleCompleted(appointment.id)}
                            >
                                Completed
                            </button>
                        </td>}
                    </tr>
                    );
                })}
                </tbody>}
            </table>
            </div>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/appointments/new" className="btn btn-primary btn-lg px-4 gap-3">Add a new appointment</Link>
            </div>
        </div>
    </div>
    );
}

export default AppointmentList;
