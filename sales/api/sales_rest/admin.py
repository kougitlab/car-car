from django.contrib import admin

# Register your models here.
from .models import Sales, SalesPerson, Customer


admin.site.register(Sales)
admin.site.register(SalesPerson)
admin.site.register(Customer)
