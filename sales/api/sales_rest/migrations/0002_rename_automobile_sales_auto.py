# Generated by Django 4.0.3 on 2023-03-07 01:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sales',
            old_name='automobile',
            new_name='auto',
        ),
    ]
