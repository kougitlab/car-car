from django.db import models
from django.urls import reverse
# Create your models here.


class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False, null=False)
    import_href = models.CharField(max_length=200, unique=True)
    
    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})
    
class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=150)
    phone_number = models.CharField(max_length=50)
    
    def __str__(self):
        return self.name
    
class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name
    
class Sales(models.Model):
    price = models.DecimalField(max_digits=100, decimal_places=2)
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE
    )
    auto = models.OneToOneField( #similar to conference go, attendees is OneToOne, 1 shopping card, 1 customer
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
    )