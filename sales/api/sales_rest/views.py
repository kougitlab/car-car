from django.shortcuts import render
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .models import Customer, Sales, SalesPerson, AutomobileVO
from django.http import JsonResponse
import json
# Create your views here. Kevin was here.

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href", "color", "year", "vin", "sold", "id",]

class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number", "id",]    

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "phone_number", "id",]
    
class SalesListEncoder(ModelEncoder):
    model = Sales
    properties = ["price", "sales_person", "customer", "auto", "id",]
    
    encoders= {
        "sales_person": SalesPersonListEncoder(),
        "customer": CustomerListEncoder(),
        "auto": AutomobileVODetailEncoder(),
    }
    
    def get_extra_data(self, o):
        return {"auto": o.auto.vin, "sales_person": o.sales_person.name, "customer": o.customer.name, "sales_person_employee_number": o.sales_person.employee_number,}
    
    
class SalesDetailEncoder(ModelEncoder):
    model = Sales
    properties = ["price", "sales_person", "customer", "auto", "id",]
    
    encoders= {
        "auto": AutomobileVODetailEncoder(),
    }
    

    

@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        salespersons = SalesPerson.objects.all()
        return JsonResponse({"salespersons": salespersons}, encoder = SalesPersonListEncoder, safe=False,)
    else:
        content =json.loads(request.body)
        try:
            salesperson = SalesPerson.objects.create(**content)
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Invalid salesperson, does not exist"}, status=400,)
            
        return JsonResponse(
            salesperson,
            encoder=SalesPersonListEncoder,
            safe=False,
        )       

@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({"customers": customers}, encoder = CustomerListEncoder, safe=False,)
    else:
        content =json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Invalid customer, does not exist"}, status=400,)
            
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False,
        )
        



        
@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sales.objects.all()
        return JsonResponse({"sales": sales}, encoder = SalesListEncoder, safe=False,)

    
    else:
        
        content = json.loads(request.body)
        
        try:
            salesperson = SalesPerson.objects.get(name=content["sales_person"])
            content["sales_person"] = salesperson
        except Sales.DoesNotExist:
            return JsonResponse({"message": "Invalid salesperson, does not exist"}, status=400,)
        
        
        try:
            customer = Customer.objects.get(name=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Invalid customer, does not exist"}, status=400,)
        
        
        
        auto_vo_vin = AutomobileVO.objects.get(vin=content["auto"])
        
        content["auto"] = auto_vo_vin
        
        try:
            if auto_vo_vin.sold == True:
                return JsonResponse({"message": "Car already sold!"}, status=400,)
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Invalid automobile VIN, does not exist"}, status=400)
        
        
        sold_status = content["auto"].sold
        
        if sold_status == False:
            content["auto"].sold = True
            auto_vo_vin.save()
            

            sale = Sales.objects.create(**content)
            return JsonResponse(
                {"sale": sale},
                encoder=SalesListEncoder,
                safe=False,
            )
        #i have unique = true for my VO model, so they cannot sell the same car with same vin
        
@require_http_methods(["GET"])
def api_show_salesperson(request, id):
    if request.method == "GET":
        salesperson = SalesPerson.objects.get(id=id)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonListEncoder,
            safe=False,
        )
        
@require_http_methods(["GET"])
def api_show_saleshistory(request, id):
      if request.method == "GET":
        sales_person = SalesPerson.objects.get(id=id)
        sales = Sales.objects.filter(sales_person=sales_person.id)
        return JsonResponse(
            {"sales": sales},
            encoder=SalesListEncoder,
            safe=False,
        )
        
        
@require_http_methods(["GET"])
def api_show_customer(request, id):
    if request.method == "GET":
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False,
        )
        
@require_http_methods(["GET"])       
def api_list_autos(request):
    if request.method == "GET":
        autos = AutomobileVO.objects.all()
        return JsonResponse(
            {"autos": autos},
            encoder=AutomobileVODetailEncoder,
            safe=False,
        )

