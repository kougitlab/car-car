import React, { useState } from 'react';

function ReviewForm() {
    const [formData, setFormData] = useState({
        contact_info: '',
        stars:'',
        description:'',
        date:'',
    })
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [formSubmitted, setFormSubmitted] = useState(false);


    let formClasses = '';
    if (formSubmitted && showSuccessMessage) {
        formClasses = 'd-none';
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8100/api/reviews/`;

        const fetchConfig = {
            method: "post",

            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
        setShowSuccessMessage(true);
        setFormSubmitted(true);
        setFormData({
            name: '',

        });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;


        setFormData({
        ...formData,

        [inputName]: value
        });
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new review</h1>
            <form onSubmit={handleSubmit} id="create-review-form" className={formClasses}>
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.contact_info}placeholder="Email" required type="text" name="contact_info" id="contact_info" className="form-control" />
                <label htmlFor="contact_info">Contact Email</label>
                </div>

                <div className="mb-3 col-auto justify-content-center">
                <div>How did we do? 1-Bad 5-Excellent</div>
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <input onChange={handleFormChange} value={1} placeholder="Stars" required type="radio" name="stars" id="stars1" className="radio-inline" style={{ margin: '0 5px' }}/>
                <label htmlFor="stars1">1</label>

                <input onChange={handleFormChange} value={2}placeholder="Stars" required type="radio" name="stars" id="stars2" className="radio-inline x-5" style={{ margin: '0 5px' }}/>
                <label htmlFor="stars2">2</label>
                <input onChange={handleFormChange} value={3}placeholder="Stars" required type="radio" name="stars" id="stars3" className="radio-inline"style={{ margin: '0 5px' }} />
                <label htmlFor="stars3">3</label>
                <input onChange={handleFormChange} value={4}placeholder="Stars" required type="radio" name="stars" id="stars4" className="radio-inline" style={{ margin: '0 5px' }} />
                <label htmlFor="stars4">4</label>
                <input onChange={handleFormChange} value={5}placeholder="Stars" required type="radio" name="stars" id="stars5" className="radio-inline" style={{ margin: '0 5px' }}/>
                <label htmlFor="stars5">5</label>
                </div>
                </div>
                <div className="form-group mb-3">
                <label htmlFor="description">Description</label>
                <textarea onChange={handleFormChange} value={formData.description}placeholder="Description" required type="text" name="description" id="description" className="form-control" rows="5" />

                </div>
                <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.date}placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                <label htmlFor="date">Date</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            <div className={`alert ${showSuccessMessage ? 'alert-success' : 'd-none'} mb-0`} id="success-message">
                Thank you for your feedback!
                </div>
            </div>
        </div>
        </div>
    )
    }


export default ReviewForm;
