import React from 'react';


export default function Footer() {
  return (
    <footer id="footer" className="text-muted">
    <section className='mt-4'>
      <form>
        <div className="row justify-content-center">
            <div className="col-auto mb-4 mb-md-0">
            <p className="pt-2 mt-4"><strong>Sign up for our newsletter</strong></p>
            </div>
            <div className="col-md-5 col-12 mb-4 mb-md-0 mt-3">
            <div className="form-floating">
                <input type="email" className="form-control" id="floatingInput" placeholder="name@example.com" />
                <label htmlFor="floatingInput">Email address</label>
            </div>
            </div>
            <div className="col-auto mb-4 mb-md-0">
            <button type="submit" className="btn btn-outline-secondary mt-4">Subscribe</button>
            </div>
        </div>
        </form>
      </section>

      <section className=''>
        <div className='container text-center text-md-start mt-5'>
          <div className='row mt-3'>
            <div className='col-md-3 col-lg-4 col-xl-3 mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>
                {/* <FontAwesomeIcon icon={faGem} color='secondary' className='me-3' /> */}
                CarCar
              </h6>
              <p>
                Here you can use rows and columns to organize your footer content. Lorem ipsum dolor sit
                amet, consectetur adipisicing elit.
              </p>
            </div>

            <div className='col-md-2 col-lg-2 col-xl-2 mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Products</h6>
              <p>
                <a href='#!' className='text-reset'>
                  Product1
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Product2
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Product3
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Product4
                </a>
              </p>
            </div>

            <div className='col-md-3 col-lg-2 col-xl-2 mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Useful links</h6>
              <p>
                <a href='#!' className='text-reset'>
                  link1
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  link2
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  link3
                  </a>
                </p>
                <p>
                <a href='#!' className='text-reset'>
                    link4
                </a>
                </p>
            </div>
            <div className='col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4'>
                <h6 className='text-uppercase fw-bold mb-4'>Contact</h6>
                <p>
                <i className='bi bi-house-door-fill me-2'></i>
                New York, NY 12345, US
                </p>
                <p>
                <i className='bi bi-envelope-fill me-3'></i>
                JohnSmith@gmail.com
                </p>
                <p>
                <i className='bi bi-telephone-fill me-3'></i>+ 01 234 567 88
                </p>
                <p>
                <i className='bi bi-printer-fill me-3'></i>+ 01 234 567 89
                </p>
            </div>
            </div>
        </div>
        </section>
        <div className='text-center p-2 ml-0' style={{ backgroundColor: 'rgba(0, 0, 0, 0.05)' }}>
            © CarCar 2023
        </div>
        </footer>
)}


//this is code that will allow us to put github facebook twitter icons and links to it, gotta get this working later



      {/* <section className='d-flex justify-content-center justify-content-lg-between p-3 border-bottom'> */}
        {/* <div className='me-5 d-none d-lg-block'>
          <span>Get connected with us on social networks:</span>
        </div> */}

        {/* <div>
        insert the icons here!!!!
          <a href='' className='me-4 text-reset'>
            <FontAwesomeIcon icon={faFacebookF} color='secondary' />
          </a>
          <a href='' className='me-4 text-reset'>
            <FontAwesomeIcon icon={faTwitter} color='secondary' />
          </a>
          <a href='' className='me-4 text-reset'>
            <FontAwesomeIcon icon={faGoogle} color='secondary' />
          </a>
          <a href='' className='me-4 text-reset'>
            <FontAwesomeIcon icon={faInstagram} color='secondary' />
          </a>
          <a href='' className='me-4 text-reset'>
            <FontAwesomeIcon icon={faLinkedin} color='secondary' />
          </a>
          <a href='' className='me-4 text-reset'>
            <FontAwesomeIcon icon={faGithub} color='secondary' />
          </a>
        </div> */}
      {/* </section> */}