import { useEffect, useState } from "react";
import { Link } from 'react-router-dom';

function SalesList() {
    const [sales, setSales] = useState([]);
    const [salesperson, setSalesPerson] = useState('');
    const [salespersons, setSalesPersons] = useState([]);

    const handleSalesPersonChange = (event) => {
        const value = event.target.value
        setSalesPerson(value);
    }

    const fetchData = async () => {
        const url = "http://localhost:8090/api/sales/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }



    useEffect(() => {
    fetchData(); 

    }, []);

    return (
    <div className="container">
        <h1>List of Sales Record</h1>
        <div className="row">
            <div className="col-sm">
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Sale Person</th>
                    <th>Employee number</th>
                    <th>Customer</th>
                    <th>Vin</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                {sales.map(sale => {
                    
                    return (
                    <tr key={sale.id}>
                        <td>{ sale.sales_person }</td>
                        <td>{sale.sales_person_employee_number}</td>
                        <td>{sale.customer}</td>
                        <td>{sale.auto}</td>
                        <td>{sale.price}</td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
            </div>
        </div>
        <Link to="/sales/new" className="mt-3 btn btn-primary">Add a sales record</Link>
    </div>
    );
    }

export default SalesList;