import { useEffect, useState } from "react";
import { Link } from 'react-router-dom';

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
    fetchData();
    }, []);

    return (
    <div className="container">
        <div className="row">
            <div className="col-sm">
            <h1>Manufacturers</h1>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                {manufacturers.map(manufacturer => {
                    return (
                    <tr key={manufacturer.id}>
                        <td>{ manufacturer.name }</td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
            </div>
        </div>
        <Link to="/manufacturers/new" className="mt-3 btn btn-primary mb-3">Add a new manufacturer</Link>
    </div>
    );
}

export default ManufacturerList;
